#!/bin/sh

# Make sure the directory for individual app logs exists
mkdir -p /var/log/shiny-server
mkdir -p /var/lib/shiny-server/bookmarks
chown shiny.shiny /var/log/shiny-server
chown shiny.shiny /var/lib/shiny-server/bookmarks
su shiny -c "shiny-server"
